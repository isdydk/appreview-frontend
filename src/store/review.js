import {rest} from "@/store/index.js";
import Axios from 'axios';

export default {
  state: {
    reviewList: null,
    reviewPage: 1,
    reviewPages: 10,
    reviewLimit: 10,
    reviewOrder: 'created_at',
    reviewSort: 'desc',
  },
  mutations: {
    setReviewList(state, payload) {
      state.reviewList = payload;
    },
    setReviewSort(state, payload) {
      state.reviewSort = payload;
    },
    setReviewPage(state, payload) {
      state.reviewPage = payload;
    },
    setReviewPages(state, payload) {
      state.reviewPages = payload;
    },
  },
  actions: {
    // Получаем список
    actionReviewList({ commit, state }) {
    
        return fetch(`${rest}/review?limit=${state.reviewLimit}&page=${state.reviewPage}&order=${state.reviewOrder}&sort=${state.reviewSort}`)
            .then(request => request.json())
            .then(data => {
              commit("setReviewList", data.result);
              commit("setReviewPages", data.pages);
            })
            .catch(err => console.error(err));

    },
    // Меняем направление сортировки
    actionReviewSort({ commit }, value) {
        
        if (value != 'asc' && value != 'desc')
          return false;
        
        commit("setReviewSort", value);
          
        return true;
    },
    actionReviewPage({ commit, state }, value) {
        
        if (value < 1 || value > state.reviewPages)
          return false;
        
        commit("setReviewPage", value);
          
        return true;
    },
    // Отправляем на  валидацию
    actionReviewValid(store, formData) {
        return Axios.post(`${rest}/review/valid`, formData)
        .then(request => request.data)
        .catch(err => console.error(err));
    },
    // 
    actionReviewAdd(store, formData) {
        return Axios.post(`${rest}/review/add`, formData)
        .then(request => request.data)
        .catch(err => console.error(err));
    },
  },
  getters: {
    getReviewList(state) {
      return state.reviewList;
    },
    getReviewSort(state) {
      return state.reviewSort;
    },
    getReviewPage(state) {
      return state.reviewPage;
    },
    getReviewPages(state) {
      return state.reviewPages;
    },
  }
};
