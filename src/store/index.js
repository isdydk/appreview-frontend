import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

import review from "./review";

export const domain = "https://appreview.ylkwb.ru";

export const rest = `${domain}/api`;

export default new Vuex.Store({
  modules: {
    review,
  },
  getters: {
    getRest() {
      return rest;
    },
    getDomain() {
      return domain;
    },
  }
});
